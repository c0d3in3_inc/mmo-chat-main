package com.c0d3in3.mmo_chat.utils

import android.content.Context
import android.widget.Toast
import com.c0d3in3.mmo_chat.model.MessageModel
import com.c0d3in3.mmo_chat.model.UserModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ktx.database
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

fun sendMessageToUser(sender : String, receiver : String, message : String, context : Context){

    if(message.isEmpty() || message.length > 128 || message.isBlank()) return Toast.makeText(context, "Message shouldn't be empty or longer than 128 characters!", Toast.LENGTH_SHORT).show()

    val database = Firebase.database
    val timeStamp = System.currentTimeMillis()
    val mUsers = listOf(sender, receiver)
    val mSorted = mUsers.sortedWith(compareBy { it})
    val senderName = FirebaseAuth.getInstance().currentUser?.displayName
    val myRef = database.getReference("messages/${mSorted[0]}_${mSorted[1]}/$timeStamp")

    myRef.setValue(MessageModel("${mSorted[0]}_${mSorted[1]}", "$timeStamp", sender, senderName.toString(),   message, "TEXT", timeStamp))

}
