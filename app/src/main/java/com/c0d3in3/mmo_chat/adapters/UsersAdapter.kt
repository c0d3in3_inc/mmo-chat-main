package com.c0d3in3.mmo_chat.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.c0d3in3.mmo_chat.R
import com.c0d3in3.mmo_chat.model.UserModel
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.user_list_item.view.*

class UsersAdapter(private val users : ArrayList<UserModel>, private val listener : UsersListener) : RecyclerView.Adapter<UsersAdapter.ViewHolder>() {

    interface UsersListener{
        fun startChat(userId: String)
    }

    val callback = listener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.user_list_item, parent, false))
    }

    override fun getItemCount(): Int = users.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.userName.text = users[position].userName
        holder.message.setOnClickListener {
            callback.startChat(users[position].userId)
        }
    }

    class ViewHolder(view : View) : RecyclerView.ViewHolder(view){

        val userImage: CircleImageView = view.userImage
        val userName: TextView = view.userNameTextView
        val message : ImageView = view.messageImageView
    }

}