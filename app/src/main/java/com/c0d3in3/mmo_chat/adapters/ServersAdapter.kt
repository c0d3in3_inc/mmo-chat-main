package com.c0d3in3.mmo_chat.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.c0d3in3.mmo_chat.R
import com.c0d3in3.mmo_chat.model.Servers
import kotlinx.android.synthetic.main.server_list_item.view.*
import java.text.SimpleDateFormat
import java.util.*


class ServersAdapter(val servers : List<Servers>, val context : Context) : RecyclerView.Adapter<ServersAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.server_list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return servers.size
    }


    class ViewHolder(view : View) : RecyclerView.ViewHolder(view){
        val serverName : TextView = view.serverNameTextView
        val chronicle : TextView = view.chronicleTextView
        val rateXp : TextView = view.rateXpTextView
        val rateSp : TextView = view.rateSpTextView
        val rateAdena : TextView = view.rateAdenaTextView
        val rateDrop : TextView = view.rateDropTextView
        val rateDropChance : TextView = view.rateDropChanceTextView
        val rateSpoil : TextView = view.rateSpoilTextView
        val startDate : TextView = view.startDateTextView

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val sfd = SimpleDateFormat(
            "dd-MM-yyyy",
            Locale.getDefault()
        )
        val date: String = sfd.format(servers[position].startDate.toDate())

        holder.serverName.text = servers[position].website
        holder.chronicle.text = servers[position].chronicle
        holder.startDate.text = "Opened: $date"
        //holder.rateXp.text = servers[position].rates[Rates[xp]]
    }
}