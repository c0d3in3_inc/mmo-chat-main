package com.c0d3in3.mmo_chat.adapters

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.c0d3in3.mmo_chat.R
import com.c0d3in3.mmo_chat.model.MessageModel
import com.c0d3in3.mmo_chat.model.User
import kotlinx.android.synthetic.main.chat_list_item.view.*

class ChatAdapter(private val messages: ArrayList<MessageModel>, private val context: Context, listener: ChatListener) : RecyclerView.Adapter<ChatAdapter.ViewHolder>() {

    interface ChatListener{

        fun removeMessage(reference : String, message : String, position : Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.chat_list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return messages.size
    }

    private val callback = listener

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.sender.text = messages[position].senderName
        if(messages[position].messageType == "REMOVED"){
            holder.message.text = "< Message was removed >"
            holder.message.setTextColor(Color.RED)
        }
        else{
            holder.message.text = messages[position].message
            holder.message.setTextColor(Color.parseColor("#0C1618"))
        }
        if(position == 0 ) holder.userLayout.visibility = View.VISIBLE
        if(position != 0 && messages[position-1].senderId == messages[position].senderId) holder.userLayout.visibility = View.GONE

        if(messages[position].senderId == User().userId && messages[position].messageType != "REMOVED"){
            holder.message.setOnLongClickListener{
                callback.removeMessage("messages/${messages[position].chatId}/${messages[position].messageId}", messages[position].message, position)
                return@setOnLongClickListener true
            }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val sender : TextView = view.senderNameTextView
        val message : TextView = view.messageTextView
        val userLayout : LinearLayout = view.userLayout
    }
}