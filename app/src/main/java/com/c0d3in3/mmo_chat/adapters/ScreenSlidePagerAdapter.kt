package com.c0d3in3.mmo_chat.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

class ScreenSlidePagerAdapter(fragmentActivity: FragmentActivity) : FragmentStateAdapter(fragmentActivity) {

    private val arrayList: ArrayList<Fragment> = ArrayList()

    fun addFragment(fragment: Fragment?) {
        arrayList.add(fragment!!)
    }

    override fun getItemCount(): Int = arrayList.size

    override fun createFragment(position: Int): Fragment {
        return arrayList[position]
    }
}