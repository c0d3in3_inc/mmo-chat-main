package com.c0d3in3.mmo_chat.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.c0d3in3.mmo_chat.R
import com.c0d3in3.mmo_chat.activity.SignActivity
import kotlinx.android.synthetic.main.fragment_sign_in.*

class SignInFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_sign_in, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        signUpTextView.setOnClickListener {
            (activity as SignActivity).mPager.currentItem = (activity as SignActivity).mPager.currentItem + 1

        }

        signInButton.setOnClickListener {
            (activity as SignActivity).signIn()
        }
        super.onViewCreated(view, savedInstanceState)
    }
}