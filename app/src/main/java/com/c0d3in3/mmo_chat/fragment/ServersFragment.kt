package com.c0d3in3.mmo_chat.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.c0d3in3.mmo_chat.R
import com.c0d3in3.mmo_chat.adapters.ServersAdapter
import com.c0d3in3.mmo_chat.model.Servers
import com.c0d3in3.mmo_chat.utils.games
import com.c0d3in3.mmo_chat.utils.lineage2
import com.c0d3in3.mmo_chat.utils.servers
import com.google.firebase.Timestamp
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.fragment_servers.*

class ServersFragment(context : Context) : Fragment(){

    private val mContext = context
    private val serversList = arrayListOf<Servers>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_servers, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        serversRecyclerView.layoutManager = LinearLayoutManager(activity)
        serversRecyclerView.adapter = ServersAdapter(serversList, mContext)

        getLineage2Servers()
    }

    private fun getLineage2Servers(){

        val db = Firebase.firestore

        serversList.clear()
        db.collection(games).document(lineage2).collection(servers).get().addOnSuccessListener { documents ->
            for(server in documents){
                val chronicle = server.get("Chronicle").toString()
                val game = lineage2
                val rates = server.get("Rates") as Map<*, *>
                val votes = server.get("Votes").toString().toLong()
                val startDate = server.get("startDate") as Timestamp
                val website = server.get("website").toString()

                serversList.add(Servers(game, chronicle, rates, votes, startDate, website))
            }
            if(serversRecyclerView != null) serversRecyclerView.adapter?.notifyDataSetChanged()
        }
    }
}