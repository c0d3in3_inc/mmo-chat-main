package com.c0d3in3.mmo_chat.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.c0d3in3.mmo_chat.R
import com.c0d3in3.mmo_chat.activity.SignActivity
import kotlinx.android.synthetic.main.fragment_sign_up.*

class SignUpFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_sign_up, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        signInTextView.setOnClickListener {
            (activity as SignActivity).mPager.currentItem = (activity as SignActivity).mPager.currentItem - 1

        }

        signUpButton.setOnClickListener {
            (activity as SignActivity).signUp()
        }
        super.onViewCreated(view, savedInstanceState)
    }
}