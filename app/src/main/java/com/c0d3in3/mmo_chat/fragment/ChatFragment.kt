package com.c0d3in3.mmo_chat.fragment

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.c0d3in3.mmo_chat.R
import com.c0d3in3.mmo_chat.activity.MainActivity
import com.c0d3in3.mmo_chat.adapters.ChatAdapter
import com.c0d3in3.mmo_chat.model.MessageModel
import com.c0d3in3.mmo_chat.model.User
import com.c0d3in3.mmo_chat.utils.sendMessageToUser
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.dialog_message_delete.view.*
import kotlinx.android.synthetic.main.fragment_chat.*

class ChatFragment(context : Context, private val receiver: String) : Fragment(), ChatAdapter.ChatListener {

    private val mContext = context
    private lateinit var mListener : ChildEventListener
    private val chatItems = arrayListOf<MessageModel>()
    private val db = Firebase.database
    private val mUsers = listOf(User().userId, receiver)
    private val mSorted = mUsers.sortedWith(compareBy { it})
    private val myRef = db.getReference("messages/${mSorted[0]}_${mSorted[1]}")
    val adapter = ChatAdapter(chatItems, mContext, this)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_chat, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUsernameLabel(receiver)

        chatRecyclerView.layoutManager = LinearLayoutManager(mContext)

        chatRecyclerView.adapter = adapter

        mListener = myRef.addChildEventListener(object : ChildEventListener{
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {

            }

            override fun onChildRemoved(p0: DataSnapshot) {

            }

            override fun onChildChanged(data: DataSnapshot, p1: String?) {
                var position = 0
                val mData = data.value as Map<*,*>
                val mMessage = MessageModel(mData["chatId"].toString(), mData["messageId"].toString(), mData["senderId"].toString(),
                    mData["senderName"].toString(), mData["message"].toString(), mData["messageType"].toString(), mData["timeStamp"].toString().toLong())
                for(message in chatItems){
                    if(message.messageId == mMessage.messageId){
                        chatItems[position] = mMessage
                        break
                    }
                    position++
                }
                adapter.notifyItemChanged(position)
            }

            override fun onChildAdded(data: DataSnapshot, child: String?) {
                val mData = data.value as Map<*,*>
                val mMessage = MessageModel(mData["chatId"].toString(), mData["messageId"].toString(), mData["senderId"].toString(),
                    mData["senderName"].toString(), mData["message"].toString(), mData["messageType"].toString(), mData["timeStamp"].toString().toLong())
                chatItems.add(mMessage)
                adapter.notifyItemInserted(chatItems.size-1)
                chatRecyclerView.scrollToPosition(chatItems.size-1)
            }
        })

        sendButton.setOnClickListener {
            sendMessageToUser(User().userId, receiver, chatEdiText.text.toString(), mContext)
            chatEdiText.text.clear()
        }

        chatEdiText.setOnClickListener {
            //chatRecyclerView.scrollToPosition(chatItems.size-1)
        }

    }

    override fun onDestroy() {
        myRef.removeEventListener(mListener)
        super.onDestroy()
    }

    class RemoveMessageDialog(reference: String, message: String, position: Int) : DialogFragment() {
        private val db = Firebase.database
        private val myRef = db.getReference(reference)
        private val mMessage = message
        private val mPosition = position
        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            return activity?.let {
                val builder = AlertDialog.Builder(it)
                // Get the layout inflater
                val inflater = requireActivity().layoutInflater

                // Inflate and set the layout for the dialog
                // Pass null as the parent view because its going in the dialog layout
                val view = inflater.inflate(R.layout.dialog_message_delete, null)
                view.messageDeleteTextView.text = mMessage
                builder.setView(view)
                    // Add action buttons
                    .setPositiveButton("Delete"
                    ) { _, _ ->
                        myRef.child("messageType").setValue("REMOVED")
                    }
                    .setNegativeButton("Cancel"
                    ) { dialog, _ ->
                        dialog?.cancel()
                    }
                builder.create()
            } ?: throw IllegalStateException("Activity cannot be null")
        }
    }

    override fun removeMessage(reference: String, message: String, position: Int
    ) {
        (activity as MainActivity).removeMessage(reference, message, position)
    }


    private fun setUsernameLabel(id : String){
        val db = Firebase.firestore
        db.collection("users").document(id).get().addOnSuccessListener {
            (activity as MainActivity).changeLabel(it.get("user_username").toString())
        }
    }
}
