package com.c0d3in3.mmo_chat.fragment

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.c0d3in3.mmo_chat.R
import com.c0d3in3.mmo_chat.activity.MainActivity
import com.c0d3in3.mmo_chat.adapters.UsersAdapter
import com.c0d3in3.mmo_chat.model.UserModel
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.fragment_users.*

class UsersFragment : Fragment(), UsersAdapter.UsersListener {

    private val usersList = ArrayList<UserModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_users, container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        (activity as MainActivity).changeLabel("Users")

        usersRecyclerView.layoutManager = LinearLayoutManager(context)
        usersRecyclerView.adapter = UsersAdapter(usersList, this)

        val db = Firebase.firestore

        usersList.clear()
        db.collection("users").get().addOnSuccessListener {
            for(user in it){
                usersList.add(UserModel(user.id, user.get("user_username").toString()))
            }
            if(usersRecyclerView != null) (usersRecyclerView.adapter as UsersAdapter).notifyDataSetChanged()
        }

        super.onViewCreated(view, savedInstanceState)
    }

    override fun startChat(userId: String) {
        (activity as MainActivity).startChat(userId)
    }
}