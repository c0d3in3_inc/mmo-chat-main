package com.c0d3in3.mmo_chat.activity


import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.c0d3in3.mmo_chat.R
import com.c0d3in3.mmo_chat.fragment.ChatFragment
import com.c0d3in3.mmo_chat.fragment.ServersFragment
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val transactions = supportFragmentManager.beginTransaction().apply {
            replace(R.id.mainFrameLayout, ServersFragment(this@MainActivity))
            addToBackStack(null)
        }
        transactions.commit()

        init()
    }

    private fun init(){
        userProfile.setOnClickListener {
//            val transactions = supportFragmentManager.beginTransaction().apply {
//                replace(R.id.mainFrameLayout, UsersFragment())
//                addToBackStack(null)
//            }
//            transactions.commit()

            val intent = Intent(this, UserProfileActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
    }

    fun removeMessage(reference : String, message : String, position : Int){
        val dialog = ChatFragment.RemoveMessageDialog(reference, message, position)
        dialog.show(supportFragmentManager, "removeMessage")
    }

    fun changeLabel(label : String){
        labelTextView.text = label
    }

    fun startChat(userId : String){
        val transactions = supportFragmentManager.beginTransaction().apply {
            replace(R.id.mainFrameLayout, ChatFragment(this@MainActivity, userId))
        }
        transactions.commit()
    }
}


