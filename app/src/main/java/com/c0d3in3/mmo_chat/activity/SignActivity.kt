package com.c0d3in3.mmo_chat.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.c0d3in3.mmo_chat.R
import com.c0d3in3.mmo_chat.fragment.SignInFragment
import com.c0d3in3.mmo_chat.fragment.SignUpFragment
import com.c0d3in3.mmo_chat.utils.DepthPageTransformer
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.auth.UserProfileChangeRequest.*
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.fragment_sign_in.*
//import kotlinx.android.synthetic.main.fragment_sign_in.emailEditText
//import kotlinx.android.synthetic.main.fragment_sign_in.passwordEditText
import kotlinx.android.synthetic.main.fragment_sign_up.*
import java.text.SimpleDateFormat
import java.util.*
import androidx.viewpager2.widget.ViewPager2
import com.c0d3in3.mmo_chat.adapters.ScreenSlidePagerAdapter
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import kotlin.properties.Delegates

class SignActivity : AppCompatActivity() {

    lateinit var mPager: ViewPager2
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign)
        // Instantiate a ViewPager and a PagerAdapter.
        mPager = findViewById(R.id.signPager)

        mPager.setPageTransformer(DepthPageTransformer())

        // The pager adapter, which provides the pages to the view pager widget.
        val pagerAdapter = ScreenSlidePagerAdapter(this)
        pagerAdapter.addFragment(SignInFragment())
        pagerAdapter.addFragment(SignUpFragment())
        mPager.adapter = pagerAdapter

    }

    fun signUp(){
        // Initialize Firebase Auth
        val auth = FirebaseAuth.getInstance()
        val db = Firebase.firestore


        val email = emailSignUpEditText.text.toString()
        val password = passwordSignUpEditText.text.toString()
        val confirmPassword = confirmPasswordEditText.text.toString()
        val username = usernameEditText.text.toString()


        if(email.isEmpty() || username.isEmpty() || password.isEmpty()) return Toast.makeText(this, "Fields must not be empty!", Toast.LENGTH_SHORT).show()
        if(password != confirmPassword) return Toast.makeText(this, "Passwords doesn't match!", Toast.LENGTH_SHORT).show()
        if(password.length < 6 || password.length > 32) return Toast.makeText(this, "Password length must be min 6 and max 32!",Toast.LENGTH_SHORT).show()
        val sdf = SimpleDateFormat("dd/M/yyyy")
        val currentDate = sdf.format(Date())


        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("signUp", "createUserWithEmail:success")

                    val user = FirebaseAuth.getInstance().currentUser

                    val profileUpdates = Builder()
                        .setDisplayName(username)
                        .build()

                    user?.updateProfile(profileUpdates)
                        ?.addOnCompleteListener { _task ->
                            if (_task.isSuccessful) {
                                Log.d("profileUpdate", "User profile updated.")
                            }
                        }

                    val userData = hashMapOf(
                        "user_username" to username,
                        "user_registration_date" to currentDate,
                        "user_followers" to "",
                        "user_following" to "",
                        "user_medals" to "",
                        "user_avatar" to "",
                        "user_id" to user?.uid
                    )

                    // Add a new document with a generated ID
                    db.collection("users").document(user?.uid.toString())
                        .set(userData)
                        .addOnSuccessListener {
                            Log.d("AddUserData", "DocumentSnapshot added with ID: $username successfully")
                        }
                        .addOnFailureListener { e ->
                            Log.w("AddUserData", "Error adding document", e)
                        }

                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    Log.w("signUp", "createUserWithEmail:failure", task.exception)
                    Toast.makeText(this, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                }

                // ...
            }

    }

    fun signIn(){
        // Initialize Firebase Auth
        val auth = FirebaseAuth.getInstance()

        val email = emailEditText.text.toString()
        val password = passwordEditText.text.toString()

        if(email.isEmpty() || password.isEmpty()) return Toast.makeText(this, "Fields must not be empty!", Toast.LENGTH_SHORT).show()

        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("signIn", "signInWithEmail:success")
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("signIn", "signInWithEmail:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                }

                // ...
            }
    }
    override fun onBackPressed() {
        if (mPager.currentItem == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed()
        } else {
            // Otherwise, select the previous step.
            mPager.currentItem = mPager.currentItem - 1
        }
    }


}
