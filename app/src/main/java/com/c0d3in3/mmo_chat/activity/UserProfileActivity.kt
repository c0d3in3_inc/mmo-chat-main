package com.c0d3in3.mmo_chat.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.c0d3in3.mmo_chat.R
import com.c0d3in3.mmo_chat.adapters.ScreenSlidePagerAdapter
import com.c0d3in3.mmo_chat.fragment.SignInFragment
import com.c0d3in3.mmo_chat.fragment.SignUpFragment
import com.c0d3in3.mmo_chat.utils.DepthPageTransformer
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.activity_user_profile.*

class UserProfileActivity : AppCompatActivity() {

    private lateinit var pagerAdapter : ScreenSlidePagerAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_profile)

        setUp()
    }

    private fun setUp(){
        profilePager.setPageTransformer(DepthPageTransformer())

        // The pager adapter, which provides the pages to the view pager widget.
        pagerAdapter = ScreenSlidePagerAdapter(this)
        pagerAdapter.addFragment(SignInFragment())
        pagerAdapter.addFragment(SignUpFragment())
        profilePager.adapter = pagerAdapter
        TabLayoutMediator(userProfileTabs, profilePager) { tab, _ ->
            profilePager.setCurrentItem(tab.position, true)
        }.attach()
        userProfileTabs.getTabAt(0)?.text = "Sign in"
        userProfileTabs.getTabAt(1)?.text = "Sign up"
    }

//    private fun addFragmentToViewPager(fragment: Fragment){
//        pagerAdapter.addFragment(fragment)
//        fragmentsList.put(fragment to )
//    }
}
