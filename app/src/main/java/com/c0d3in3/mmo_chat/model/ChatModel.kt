package com.c0d3in3.mmo_chat.model

class MessageModel(val chatId : String, val messageId : String, val senderId : String, val senderName: String, val message : String, val messageType : String, val timeStamp : Long){
}