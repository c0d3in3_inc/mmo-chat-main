package com.c0d3in3.mmo_chat.model

import com.google.firebase.auth.FirebaseAuth

class User {
    private val auth = FirebaseAuth.getInstance()
    val userId = auth.currentUser!!.uid
}