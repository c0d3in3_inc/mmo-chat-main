package com.c0d3in3.mmo_chat.model

import com.google.firebase.Timestamp

class Servers(val game: String, val chronicle: String, val rates: Map<*,*>, val votes: Long, val startDate: Timestamp, val website: String) {
}

class Rates(val xp : Long, val sp : Long, val adena : Long, val drop : Long, val dropRate : Long, val spoil : Long)